# README #

* ``` minikube start --cpus 4 --memory 8192 ``` start minikube with param
* ``` minikube dashboard ``` - open dashboard

* ``` kubectl create -f logging-namespace.yaml ```  - adds a namespace for logging components
* ``` kubectl create -f elasticsearch-service.yaml ``` - adds service for elasticsearch
* ``` kubectl create -f elasticsearch-statefulset.yaml ``` - adds stateful set for elasticsearch
* ``` kubectl port-forward es-cluster-0 9200:9200 --namespace=kube-logging ```
* ``` kubectl create -f kibana.yaml ``` - adds service and deployment for kibana
* ``` kubectl get pods --namespace=kube-logging ``` - get kibana pod name for next line. For example: kibana-5749b5778b-s7ql6 
* ``` kubectl port-forward kibana-5749b5778b-s7ql6 5601:5601 --namespace=kube-logging ```
* ``` kubectl create -f fluentd.yaml ``` - adds ServiceAccount, ClusterRole, ClusterRoleBinding and DaemonSet for fluentd
* ``` kubectl create -f comment-service.yaml ``` - adds Service and Deployment for comment service App
* ``` kubectl create -f post-service.yaml ``` - adds Service and Deployment for post service App
* ``` minikube service post-service ```  - gets url for post service App

* ``` minikube stop ``` stop minikube